package com.data;

public class DataBases {
    public static final class ChatDB{
        public static final String TABLE = "chat_table";
        public static final String CID = "cid";
        public static final String UID = "userid";
        public static final String CONTENT = "content";
        public static final String NAME = "name";
        public static final String TIME = "ctime";
        public static final String RN = "roomnum";
        public static final String  CREATE =
                "create table if not exists " + TABLE + " ("
                        + CID + " int primary key, "
                        + UID + " text not null, "
                        + NAME + " text not null, "
                        + CONTENT + " text not null, "
                        + TIME + " text not null, "
                        + RN + " int not null, "
                        + "foreign key("+RN+") references "+TABLE+"("+RN+")"
                        + "on delete cascade);";
    }

    public static final class ChatRoomDB{
        public static final String TABLE = "chatroom_table";
        public static final String RN = "roomnum";
        public static final String USERS = "users";
        public static final String  CREATE =
                "create table if not exists " + TABLE + " ("
                        + RN + " int primary key, "
                        + USERS + " text );";
    }

    public static final class UserDB{
        public static final String TABLE = "user_table";
        public static final String ID = "id";
        public static final String NAME = "name";
        public static final String PHONE = "phone";
        public static final String MSG = "msg";
        public static final String ADDR = "backgaddress";
        public static final String PADDR = "profileaddr";
        public static final String CREATE =
                "create table if not exists " + TABLE + " ("
                + ID + " text primary key, "
                + NAME + " text, "
                + PHONE +  " text, "
                + MSG + " text, "
                + ADDR + " text, "
                + PADDR + " text);";
    }
}
