package com.data;

import android.graphics.drawable.Drawable;

public class FriendListVO {
    private String state;
    private Drawable img;
    private String id; //아이디
    private String user_name; //이름
    private String phoneNum; //폰번호
    private String message; //상태메세지
    private String backgaddress; //배경 이미지주소
    private String profileaddress; //프로필 이미지주소

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Drawable getImg() {
        return img;
    }

    public void setImg(Drawable img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBackgaddress() {
        return backgaddress;
    }

    public void setBackgaddress(String backgaddress) {
        this.backgaddress = backgaddress;
    }

    public String getProfileaddress() {
        return profileaddress;
    }

    public void setProfileaddress(String profileaddress) {
        this.profileaddress = profileaddress;
    }
}
