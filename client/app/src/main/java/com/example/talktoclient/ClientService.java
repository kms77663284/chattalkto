package com.example.talktoclient;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.data.ChatVO;
import com.data.DataBases;
import com.data.DataPacket;
import com.data.SharedPreferenceUtil;
import com.data.TalkToDBOpenHelper;
import com.data.UserVO;
import com.server.MessageProtocol;
import com.server.ServerData;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;

public class ClientService extends Service {
    private SharedPreferenceUtil session;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private Boolean intoFragment;
    private TalkToDBOpenHelper tdb;

    IBinder binder = new ClientBinder();
    class ClientBinder extends Binder {
        ClientService getService() { // 서비스 객체를 리턴
            return ClientService.this;
        }
    }
    Socket socket = null;
    receiveThread receive = null;

    @Override
    public void onCreate() {
        super.onCreate();
        intoFragment = true;
        tdb = new TalkToDBOpenHelper(this, null, 1);
        try{
            if (socket == null)
                socket = new Socket(ServerData.IP, ServerData.SOCKETPORT);

            session = new SharedPreferenceUtil(getApplicationContext());

            // 서버 접속 통보
            oos = new ObjectOutputStream(socket.getOutputStream());
            DataPacket test = new DataPacket();
            test.setProtocol(MessageProtocol.SOCKET_CONNECT);
            UserVO me = new UserVO();
            me.setID(session.getSharedID());
            //me.setName(session.getSharedName());
            //me.setPhone(session.getSharedPhone());
            test.setUdata(me);
            oos.writeObject(test);

            receive = new receiveThread();
            receive.start();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        if (receive != null){receive.interrupt(); }
        try {
            oos.close();
            ois.close();
            socket.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.i("Bind", "Service Bind");
        return binder;
    }

    public void sendMSG(ChatVO chat, int room){
        try{
            DataPacket data = new DataPacket();
            SharedPreferenceUtil mydata = new SharedPreferenceUtil(this);

            data.setProtocol(MessageProtocol.CS_SEND_MSG);
            data.setRoomId(room);
            UserVO me = new UserVO();
            me.setID(mydata.getSharedID());
            me.setName(mydata.getSharedName());
            me.setPhone(mydata.getSharedPhone());
            data.setUdata(me);
            data.setCdata(chat);

            oos.writeObject(data);
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    public void sendCreateChatRoom(ArrayList<UserVO> list, boolean Frag){
        intoFragment = Frag;
        try{
            DataPacket data = new DataPacket();

            data.setProtocol(MessageProtocol.CHAT_CREATE);
            UserVO me = new UserVO();
            me.setID(session.getSharedID());
            me.setName(session.getSharedName());
            data.setUdata(me);
            data.setUserlist(list);

            oos.writeObject(data);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private class receiveThread extends Thread{
        @Override
        public void run() {
            StringBuffer buf;
            try{
                //SQLiteDatabase chatdb = cdb.getWritableDatabase();
                //SQLiteDatabase roomdb = crdb.getWritableDatabase();
                SQLiteDatabase talkdb = tdb.getWritableDatabase();
                ois = new ObjectInputStream(socket.getInputStream());
                while (!Thread.currentThread().isInterrupted()){
                    DataPacket data = (DataPacket) ois.readObject();
                    switch (data.getProtocol()) {
                        case MessageProtocol.SC_SEND_MSG:
                            ContentValues chatvalues = new ContentValues();
                            chatvalues.put(DataBases.ChatDB.UID, data.getUdata().getID());
                            chatvalues.put(DataBases.ChatDB.NAME, data.getUdata().getName());
                            chatvalues.put(DataBases.ChatDB.CONTENT, data.getCdata().getContent());
                            chatvalues.put(DataBases.ChatDB.TIME, data.getCdata().getTime());
                            chatvalues.put(DataBases.ChatDB.RN, data.getRoomId());
                            chatvalues.put(DataBases.ChatDB.CID, data.getCdata().getCID());
                            talkdb.insert(DataBases.ChatDB.TABLE, null, chatvalues);
                            Intent chatintent = new Intent("receivechat");
                            chatintent.putExtra("chat", data);
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(chatintent);
                            break;
                        case MessageProtocol.SYNC_START:
                            Intent sync = new Intent("createchat");
                            sync.putExtra("sync", "start");
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(sync);
                            break;
                        case MessageProtocol.SYNC_DONE:
                            Intent sync2 = new Intent("createchat");
                            sync2.putExtra("sync", "done");
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(sync2);
                            break;
                        case MessageProtocol.SYNC_CHATROOM:
                            ContentValues syncroom = new ContentValues();
                            syncroom.put(DataBases.ChatRoomDB.RN, data.getRoomId());
                            buf = new StringBuffer();
                            for (UserVO tmp : data.getUserlist()){
                                if (buf.length() == 0) buf.append(tmp.getID());
                                else buf.append(","+tmp.getID());
                            }
                            syncroom.put(DataBases.ChatRoomDB.USERS, buf.toString());
                            talkdb.insertWithOnConflict(DataBases.ChatRoomDB.TABLE, null, syncroom, SQLiteDatabase.CONFLICT_REPLACE);
                            break;
                        case MessageProtocol.SYNC_CHAT:
                            ContentValues syncchat = new ContentValues();
                            syncchat.put(DataBases.ChatDB.UID, data.getCdata().getUser().getID());
                            syncchat.put(DataBases.ChatDB.CONTENT, data.getCdata().getContent());
                            syncchat.put(DataBases.ChatDB.NAME, data.getCdata().getUser().getName());
                            syncchat.put(DataBases.ChatDB.TIME, data.getCdata().getTime());
                            syncchat.put(DataBases.ChatDB.RN, data.getRoomId());
                            syncchat.put(DataBases.ChatDB.CID, data.getCdata().getCID());
                            talkdb.insertWithOnConflict(DataBases.ChatDB.TABLE, null, syncchat, SQLiteDatabase.CONFLICT_IGNORE);
                            break;
                        case MessageProtocol.SOCKET_CONNECT_SUCCESS:
                            Log.i("CONNECTION", "Server connected");
                            break;
                        case MessageProtocol.CHAT_OUT:
                            // 자신이 나가는 경우 : 채팅방 기록 삭제
                            // 타인이 나가는 경우 : 채팅방 인원 갱신
                            if (data.getUdata().getID().equals(session.getSharedID())){
                                talkdb.execSQL("delete from " + DataBases.ChatRoomDB.TABLE + " where " + DataBases.ChatRoomDB.RN + "=" + data.getRoomId());
                            }else{
                                ContentValues chatout = new ContentValues();
                                chatout.put(DataBases.ChatRoomDB.RN, data.getRoomId());
                                Cursor cs = talkdb.rawQuery("select " + DataBases.ChatRoomDB.USERS + " from " + DataBases.ChatRoomDB.TABLE + " where " + DataBases.ChatRoomDB.RN + "=" + data.getRoomId(), null);
                                if (cs.moveToNext()){
                                    String u = cs.getString(cs.getColumnIndex(DataBases.ChatRoomDB.USERS));
                                    u = u.replace(data.getUdata().getID(), "");
                                    u = u.replace(",,", ",");
                                    if (u.startsWith(",")){
                                        u = u.substring(1);
                                    }
                                    if (u.endsWith(",")){
                                        u = u.substring(0,u.length()-1);
                                    }
                                    chatout.put(DataBases.ChatRoomDB.USERS, u);
                                    talkdb.update(DataBases.ChatRoomDB.TABLE, chatout, DataBases.ChatRoomDB.RN+"="+data.getRoomId(),null);
                                } else{
                                    Log.d("LOG", "roomdb users load fail");
                                }
                            }
                            break;
                        case MessageProtocol.CHAT_IN:
                            ArrayList<UserVO> list2 = data.getUserlist();
                            if (list2.size() == 1 && list2.get(0).getID().equals(session.getSharedID())){
                                Intent selfchat = new Intent("selfprofilechat");
                                selfchat.putExtra("roomnum", data.getRoomId());
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(selfchat);
                                intoFragment = true;
                            }else if(intoFragment){
                                Intent multichat = new Intent("createchat");
                                multichat.putExtra("roomnum", data.getRoomId());
                                multichat.putExtra("sync", "in");
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(multichat);
                            }
                            else{
                                Intent createchat = new Intent("profilechat");
                                createchat.putExtra("roomnum", data.getRoomId());
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(createchat);
                                intoFragment = true;
                            }
                            break;
                        case MessageProtocol.CHAT_CREATE: // roomid랑 userlist 받아옴
                            ArrayList<UserVO> list = data.getUserlist();
                            buf = new StringBuffer();
                            for (UserVO tmp : list){
                                if (buf.length() == 0)
                                    buf.append(tmp.getID());
                                else buf.append(","+tmp.getID());
                            }
                            ContentValues roomvalues = new ContentValues();
                            roomvalues.put(DataBases.ChatRoomDB.RN, data.getRoomId());
                            roomvalues.put(DataBases.ChatRoomDB.USERS, buf.toString());
                            talkdb.insertWithOnConflict(DataBases.ChatRoomDB.TABLE, null, roomvalues, SQLiteDatabase.CONFLICT_IGNORE);
                            if (list.size() == 1 && list.get(0).getID().equals(session.getSharedID())){
                                Intent selfchat = new Intent("selfprofilechat");
                                selfchat.putExtra("roomnum", data.getRoomId());
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(selfchat);
                                intoFragment = true;
                            }else if(intoFragment){
                                Intent multichat = new Intent("createchat");
                                multichat.putExtra("roomnum", data.getRoomId());
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(multichat);
                            }
                            else{
                                Intent createchat = new Intent("profilechat");
                                createchat.putExtra("roomnum", data.getRoomId());
                                LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(createchat);
                                intoFragment = true;
                            }
                            break;
                    }
                }
                //chatdb.close();
                //roomdb.close();
                talkdb.close();
            }catch (ClassNotFoundException e){
                e.printStackTrace();
            }catch (IOException e){
                Log.d("LOG","packet send fail");
                e.printStackTrace();
            }
        }
    }
}
