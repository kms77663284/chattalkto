package com.example.talktoclient;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.data.ChatVO;
import com.data.DataBases;
import com.data.SharedPreferenceUtil;
import com.data.TalkToDBOpenHelper;
import com.data.UserVO;
import com.server.ServerData;

import java.util.ArrayList;
import java.util.HashMap;

import kr.go.seoul.airquality.AirQualityTypeMini;
import kr.go.seoul.trafficbus.TrafficBusTypeMini;
import kr.go.seoul.trafficsubway.TrafficSubwayTypeMini;

public class ChatAdapter extends BaseAdapter {
    private ArrayList<ChatVO> list = new ArrayList<ChatVO>();
    private static final String URL="http://"+ ServerData.IP +":"+ServerData.JSPPORT;
    private static final String API_KEY = "627350524173616a36364661656f76";
    private SharedPreferenceUtil session;
    private TalkToDBOpenHelper tdb;
    private SQLiteDatabase db;
    private HashMap<String, String> imgMap = new HashMap<String, String>();

    public void addVO(ChatVO chat){
        if (chat.getContent().startsWith("#")) chat.setTag(true);
        else chat.setTag(false);
        list.add(chat);
    }

    public void clean(){
        list.clear();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final Context context = parent.getContext();
        if (session == null)
            session = new SharedPreferenceUtil(context);
        ChatVO chat = list.get(position);
        if (chat.getUser().getID().equals(session.getSharedID())) chat.setMe(true);
        else chat.setMe(false);
        if (tdb == null) tdb = new TalkToDBOpenHelper(context, null, 1);
        if (db == null) db = tdb.getWritableDatabase();
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (chat.isMe()) {
            if (chat.isTag()){
                switch (chat.getContent()){
                    case "#날씨":
                        convertView = inflater.inflate(R.layout.mytag_airquality, parent, false);
                        AirQualityTypeMini airtag = (AirQualityTypeMini)convertView.findViewById(R.id.m_air_tag);
                        airtag.setOpenAPIKey(API_KEY);
                        break;
                    case "#버스":
                        convertView = inflater.inflate(R.layout.mytag_trafficbus , parent, false);
                        TrafficBusTypeMini bustag = (TrafficBusTypeMini)convertView.findViewById(R.id.m_bus_tag);
                        bustag.setOpenAPIKey(API_KEY);
                        break;
                    case "#지하철":
                        convertView = inflater.inflate(R.layout.mytag_trafficsubway, parent, false);
                        TrafficSubwayTypeMini subtag = (TrafficSubwayTypeMini) convertView.findViewById(R.id.m_subway_tag);
                        subtag.setOpenAPIKey(API_KEY);
                        break;
                }
            }else {
                convertView = inflater.inflate(R.layout.myinput, parent, false);
                TextView msg2 = (TextView) convertView.findViewById(R.id.msg2);
                msg2.setText(chat.getContent());
            }
            TextView time2 = (TextView) convertView.findViewById(R.id.time2);
            time2.setText(chat.getTime());
        } else {
            if (imgMap.getOrDefault(chat.getUser().getID(), "none").equals("none")) {
                Cursor imgcur = db.rawQuery("select " + DataBases.UserDB.PADDR + " from " + DataBases.UserDB.TABLE + " where " + DataBases.UserDB.ID + "=?",
                        new String[]{chat.getUser().getID()});
                if (imgcur.moveToNext())
                    imgMap.put(chat.getUser().getID(),imgcur.getString(imgcur.getColumnIndex(DataBases.UserDB.PADDR)));
            }
            if (chat.isTag()){
                switch (chat.getContent()){
                    case "#날씨":
                        convertView = inflater.inflate(R.layout.othertag_airquality, parent, false);
                        TextView name = (TextView) convertView.findViewById(R.id.chatname);
                        ImageView img = (ImageView) convertView.findViewById(R.id.img);
                        //img.setImageDrawable(chat.getUser().getImg());
                        Glide.with(convertView).load(URL+imgMap.get(chat.getUser().getID())).into(img);
                        name.setText(chat.getUser().getName());

                        AirQualityTypeMini airtag = (AirQualityTypeMini)convertView.findViewById(R.id.o_air_tag);
                        airtag.setOpenAPIKey(API_KEY);
                        break;
                    case "#버스":
                        convertView = inflater.inflate(R.layout.othertag_trafficbus , parent, false);
                        TextView name2 = (TextView) convertView.findViewById(R.id.chatname);
                        ImageView img2 = (ImageView) convertView.findViewById(R.id.img);
                        //img2.setImageDrawable(chat.getUser().getImg());
                        Glide.with(convertView).load(URL+imgMap.get(chat.getUser().getID())).into(img2);
                        name2.setText(chat.getUser().getName());

                        TrafficBusTypeMini bustag = (TrafficBusTypeMini)convertView.findViewById(R.id.o_bus_tag);
                        bustag.setOpenAPIKey(API_KEY);
                        break;
                    case "#지하철":
                        convertView = inflater.inflate(R.layout.othertag_trafficsubway, parent, false);
                        TextView name3 = (TextView) convertView.findViewById(R.id.chatname);
                        ImageView img3 = (ImageView) convertView.findViewById(R.id.img);
                        //img3.setImageDrawable(chat.getUser().getImg());
                        Glide.with(convertView).load(URL+imgMap.get(chat.getUser().getID())).into(img3);
                        name3.setText(chat.getUser().getName());

                        TrafficSubwayTypeMini subtag = (TrafficSubwayTypeMini) convertView.findViewById(R.id.o_subway_tag);
                        subtag.setOpenAPIKey(API_KEY);
                        break;
                }
            }else{
                convertView = inflater.inflate(R.layout.otherinput, parent, false);
                TextView msg1 = (TextView) convertView.findViewById(R.id.msg);
                TextView name = (TextView) convertView.findViewById(R.id.chatname);
                ImageView img = (ImageView) convertView.findViewById(R.id.img);

                msg1.setText(chat.getContent());
                //img.setImageDrawable(chat.getUser().getImg());
                Glide.with(convertView).load(URL+imgMap.get(chat.getUser().getID())).into(img);
                name.setText(chat.getUser().getName());
            }
            TextView time1 = (TextView)convertView.findViewById(R.id.time);
            time1.setText(chat.getTime());
        }

        return convertView;
    }
}
