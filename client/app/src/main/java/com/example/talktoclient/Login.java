package com.example.talktoclient;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.data.SharedPreferenceUtil;
import com.server.ServerData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
public class Login extends AppCompatActivity {
    private static final String  URL = "http://"+ ServerData.IP+":"+ServerData.JSPPORT+"/TalkTo/LoginCheck.jsp";

    private String sp_id;
    private String sp_pwd;
    //private SharedPreferences appData;
    private SharedPreferenceUtil appData;
    private EditText userid;
    private EditText pwd;
    private Button btn1;
    private Button sign_in;

    @Override
    protected void onStart() {
        super.onStart();
        if(!userid.getText().toString().equals("") && !pwd.getText().toString().equals("")){ //설정값이 있으면 강제버튼 클릭시키기
            Log.d("LOG","둘다 값있음 버튼누르다");
            btn1.performClick();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        appData = new SharedPreferenceUtil(getApplicationContext());
        //appData = getSharedPreferences("appData", MODE_PRIVATE);
        load();

        userid = (EditText) findViewById(R.id.userid);
        pwd = (EditText) findViewById(R.id.pwd);
        btn1 = (Button) findViewById(R.id.bn_login);
        sign_in = (Button) findViewById(R.id.sign_in);

        userid.setText(sp_id);
        pwd.setText(sp_pwd);


        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), sign_in.class);
                startActivity(intent);
            }
        });
        btn1.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                String id = userid.getText().toString();
                String password = pwd.getText().toString();
                if(id.equals("")||password.equals("")){
                    Toast.makeText(v.getContext(),"아이디 또는 비밀번호를 입력해주세요",Toast.LENGTH_SHORT);
                    return;
                }
                ContentValues values= new ContentValues();
                values.put("msg","login");
                values.put("id",id);
                values.put("pwd",password);
                NetworkTask networkTask = new NetworkTask(Login.this,URL, values);
                networkTask.execute();

                save();
                finish();
            }
        });


    }
    private void save() {
        // SharedPreferences 객체만으론 저장 불가능 Editor 사용
        //SharedPreferences.Editor editor = appData.edit();

        // 에디터객체.put타입( 저장시킬 이름, 저장시킬 값 )
        // 저장시킬 이름이 이미 존재하면 덮어씌움

        //editor.putString("ID", userid.getText().toString().trim());
        //editor.putString("PWD", pwd.getText().toString().trim());

        appData.setSharedID(userid.getText().toString().trim());
        appData.setSharedPwd(pwd.getText().toString().trim());
        // apply, commit 을 안하면 변경된 내용이 저장되지 않음
        //editor.apply();
    }

    private void load() {
        // SharedPreferences 객체.get타입( 저장된 이름, 기본값 )
        // 저장된 이름이 존재하지 않을 시 기본값

        //sp_id = appData.getString("ID", "");
        //sp_pwd = appData.getString("PWD", "");
        sp_id = appData.getSharedID();
        sp_pwd = appData.getSharedPwd();
    }
}
