package com.example.talktoclient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.data.DataBases;
import com.data.FriendListVO;
import com.data.SharedPreferenceUtil;
import com.data.TalkToDBOpenHelper;
import com.data.UserVO;
import com.server.ServerData;

import java.util.ArrayList;

public class ProfileOtherActivity extends AppCompatActivity {
    private static final String URL="http://"+ ServerData.IP +":"+ServerData.JSPPORT;
    private FriendListVO f_info; //친구 (한명)정보

    private ImageView f_backimg;      //배경이미지
    private ImageView f_profileimg;   //프로필 이미지
    private ImageButton f_pfclose; //닫기 버튼
    private TextView f_statemsg;          //상태메세지 Text
    private TextView f_username;      //이름 Text
    private Button f_chatbtn;     //1대1 채팅 버튼
    private SharedPreferenceUtil session;   // 본인 정보
    private ClientService cs;      // 서비스 내부 메소드 호출용
    boolean isService = false;     // 서비스 연결여부

    // 서비스 연결 객체.
    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ClientService.ClientBinder cb = (ClientService.ClientBinder)service;
            cs = cb.getService();
            isService = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isService = false;
        }
    };

    // 서비스에서 보낸 브로드캐스트 메세지 수신 장소
    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int roomid = intent.getIntExtra("roomnum", -1);
            if (roomid == -1){
                Toast.makeText(context,"채팅방 생성에 실패하였습니다.", Toast.LENGTH_SHORT).show();
            }
            else {
                Intent intochat = new Intent(ProfileOtherActivity.this, ChatActivity.class);
                intochat.putExtra("roomid", roomid);
                startActivity(intochat);
                finish();
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(conn);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_other);

        session = new SharedPreferenceUtil(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Intent revIntent = getIntent();
        f_info = new FriendListVO();
        addInfo(revIntent);//f_info에 정보 넣어주는메소드
        f_backimg = (ImageView)findViewById(R.id.f_backimg);
        f_profileimg =(ImageView)findViewById(R.id.f_profileimg);
        f_pfclose =(ImageButton) findViewById(R.id.f_pfclose);
        f_statemsg =(TextView)findViewById(R.id.f_statemsg);
        f_username =(TextView)findViewById(R.id.f_username);
        f_chatbtn =(Button)findViewById(R.id.f_chatbtn);

        f_username.setText(f_info.getUser_name());
        f_statemsg.setText(f_info.getMessage());
        Glide.with(this).load(URL+f_info.getProfileaddress()).into(f_profileimg);
        Glide.with(this).load(URL+f_info.getBackgaddress()).into(f_backimg);
        f_chatbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkExist()){
                    UserVO other = new UserVO();
                    other.setID(f_info.getId());
                    other.setName(f_info.getUser_name());
                    other.setPhone(f_info.getPhoneNum());
                    ArrayList<UserVO> chatlist = new ArrayList<UserVO>();   // 리스트에는 자기 외의 맴버만 추가함
                    chatlist.add(other);
                    cs.sendCreateChatRoom(chatlist, false);
                }
            }
        });

        // 서비스 연결 및 브로드 캐스트 설정
        Intent bindservice = new Intent(this, ClientService.class);
        bindService(bindservice, conn, Context.BIND_AUTO_CREATE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, new IntentFilter("profilechat"));
    }
    public void addInfo(Intent intent){
        Log.d("LOG","addfriendInfo에서 f_info에 정보넣음");
        f_info.setState(intent.getStringExtra("state"));
        f_info.setId(intent.getStringExtra("id"));
        f_info.setUser_name(intent.getStringExtra("user_name"));
        f_info.setProfileaddress(intent.getStringExtra("profileaddress"));
        f_info.setMessage(intent.getStringExtra("message"));
        f_info.setPhoneNum(intent.getStringExtra("phoneNum"));
        f_info.setBackgaddress(intent.getStringExtra("backgaddress"));
    }

    public boolean checkExist(){
        //ChatRoomDBHelper crdb = new ChatRoomDBHelper(getApplicationContext(), "talkto.db", null, 1);
        TalkToDBOpenHelper tdb = new TalkToDBOpenHelper(getApplicationContext(), null, 1);
        SQLiteDatabase roomdb = tdb.getWritableDatabase();
        String sql = "select " + DataBases.ChatRoomDB.RN + " from " + DataBases.ChatRoomDB.TABLE + " where " + DataBases.ChatRoomDB.USERS + "=? OR " + DataBases.ChatRoomDB.USERS + "=?";
        Cursor cur = roomdb.rawQuery(sql, new String[]{session.getSharedID()+","+f_info.getId(), f_info.getId()+","+session.getSharedID()});
        if (cur.moveToNext()){
            Intent intochat = new Intent(ProfileOtherActivity.this, ChatActivity.class);
            intochat.putExtra("roomid", cur.getInt(cur.getColumnIndex(DataBases.ChatRoomDB.RN)));
            startActivity(intochat);
            return true;
        }
        return false;
    }
}
