package com.example.talktoclient;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.data.FriendListVO;
import com.mikepenz.iconics.IconicsDrawable;
import com.mikepenz.ionicons_typeface_library.Ionicons;
import com.server.ServerData;

import java.util.List;

public class FragmentFriend extends Fragment {
    private static final String URL="http://"+ ServerData.IP +":"+ServerData.JSPPORT;
    private static final String ADDFRIENDURL="http://"+ ServerData.IP +":"+ServerData.JSPPORT+"/TalkTo/addFriend.jsp";
    private ListView list1;
    private ListViewAdapter adapter;
    private List<FriendListVO> list;
    private FriendListVO loginInfo;
    private LinearLayout linearLayout;

    private ImageView myimg;
    private TextView myname;
    private TextView mymsg;
    private TextView f_size;



    public FragmentFriend(FriendListVO loginInfo, List<FriendListVO> list){
        this.list=list;
        this.loginInfo = loginInfo;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_friend,container,false);
        initToolbar(view);

        //xml설정과 데이터넣어주기
        linearLayout = (LinearLayout)view.findViewById(R.id.llayout) ;
        myimg = (ImageView)view.findViewById(R.id.myimg);
        myname = (TextView)view.findViewById(R.id.myname);
        mymsg = (TextView)view.findViewById(R.id.mymsg);
        f_size =(TextView)view.findViewById(R.id.friendSize);
        Glide.with(view).load(URL+loginInfo.getProfileaddress()).into(myimg);
        myname.setText(loginInfo.getUser_name());
        mymsg.setText(loginInfo.getMessage());
        f_size.setText("내친구 : "+String.valueOf(list.size())+"명");

        //친구목록 어뎁터와 리스트 설정
        adapter = new ListViewAdapter();
        list1 = (ListView)view.findViewById(R.id.list1);

        list1.setAdapter(adapter);
        list1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("LOG",((FriendListVO)adapter.getItem(position)).getUser_name()+"님을 누름");
                FriendListVO f_info = (FriendListVO)adapter.getItem(position);
                Intent intent = new Intent(getActivity(), ProfileOtherActivity.class);
                intent.putExtra("state","friend");
                intent.putExtra("id",f_info.getId());
                intent.putExtra("user_name",f_info.getUser_name());
                intent.putExtra("phoneNum",f_info.getPhoneNum());
                intent.putExtra("message",f_info.getMessage());
                intent.putExtra("backgaddress",f_info.getBackgaddress());
                intent.putExtra("profileaddress",f_info.getProfileaddress());

                startActivity(intent);

                //onPause();
            }
        });
        for(FriendListVO f_list : list){
            adapter.addVO(f_list);
        }
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("LOG","자기자신 클릭");
                Intent intent = new Intent(getActivity(), ProfileActivity.class);
                intent.putExtra("state","my");
                intent.putExtra("id",loginInfo.getId());
                intent.putExtra("user_name",loginInfo.getUser_name());
                intent.putExtra("phoneNum",loginInfo.getPhoneNum());
                intent.putExtra("message",loginInfo.getMessage());
                intent.putExtra("backgaddress",loginInfo.getBackgaddress());
                intent.putExtra("profileaddress",loginInfo.getProfileaddress());

                startActivity(intent);
            }
        });
        return view;
    }
    private void initToolbar(View view){
        if (Build.VERSION.SDK_INT >= 21) {
            // 21 버전 이상일 때
            getActivity().getWindow().setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = view.findViewById(R.id.toolbar1);

        toolbar.setTitle("");

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        TextView title = toolbar.findViewById(R.id.title);
        title.setText("친구");

        ImageButton toolBtn1 =view.findViewById(R.id.toolbarBtn1);
        Drawable img = new IconicsDrawable(view.getContext(), Ionicons.Icon.ion_android_person_add).sizeDp(24);
        toolBtn1.setImageDrawable(img);

        toolBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //친구추가 버튼 눌렀을때
                viewAlertDialog(v);
            }
        });
    }
    public void viewAlertDialog(View v){
        AlertDialog.Builder ad = new AlertDialog.Builder(v.getContext());

        ad.setTitle("친구 추가하기");       // 제목 설정
        ad.setMessage("친구의 아이디 or 폰번호를 입력해주세요.");   // 내용 설정

        // EditText 삽입하기
        final EditText et = new EditText(v.getContext());
        ad.setView(et);

        // 확인 버튼 설정
        ad.setPositiveButton("추가", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.v("LOG", "추가버튼 클릭");

                // Text 값 받아서 로그 남기기
                String value = et.getText().toString();
                Log.v("LOG" ,value+" : 값 value에저장");

                //jsp로 보내서 아이디나폰번호가있으면 친구추가 없으면 없다고 메세지
                if(value.equals("")){
                    Toast.makeText(getContext(),"아이디또는 폰번호를 입력해주세요.",Toast.LENGTH_SHORT);

                    dialog.dismiss();     //닫기
                }else{
                    addFriend(value);
                    dialog.dismiss();     //닫기
                }

                // Event
            }
        });

        // 취소 버튼 설정
        ad.setNegativeButton("닫기", new  DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.v("LOG", "닫기 버튼 클릭");

                dialog.dismiss();     //닫기
            }
        });


// 창 띄우기
        ad.show();
        Log.d("LOG","ad.show() 띄움");

    }
    public void addFriend(String value){
        ContentValues values= new ContentValues();
        values.put("msg","addFriend");
        values.put("f_code",value); //아이디 이거나 폰번호이거나 둘중하나라 code라함
        values.put("login_id",loginInfo.getId());
        Log.d("LOG","login_id: "+loginInfo.getId());
        NetworkTask networkTask = new NetworkTask(getContext(),ADDFRIENDURL, values);
        networkTask.execute();
        try {
            Thread.sleep(500);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(networkTask.getState().equals("친구추가성공")){
            list.add(networkTask.getFriend());
            adapter.addVO(networkTask.getFriend());
            Log.d("LOG","list.size():"+list.size());
            adapter.notifyDataSetChanged();

        }else{
            Toast.makeText(getContext(),"친구추가 실패!",Toast.LENGTH_SHORT);
        }
    }
}
