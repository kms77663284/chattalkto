package com.example.talktoclient;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.data.FriendListVO;
import com.server.ServerData;

import java.util.ArrayList;
import java.util.List;

public class ListViewAdapter extends BaseAdapter {

    private List<FriendListVO> listVO = new ArrayList<FriendListVO>();
    private static final String URL ="http://"+ ServerData.IP +":"+ServerData.JSPPORT;

    @Override
    public int getCount() {
        return listVO.size();
    }

    @Override
    public Object getItem(int position) {
        return listVO.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int pos = position;
        final Context context = parent.getContext();

        if(convertView == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.custom_listview, parent, false);
        }

        ImageView img = (ImageView) convertView.findViewById(R.id.img);
        TextView title = (TextView) convertView.findViewById(R.id.title);
        TextView Context = (TextView) convertView.findViewById(R.id.context);

        if(listVO.size()>0){
            FriendListVO listViewItem = listVO.get(position);
            Glide.with(convertView).load(URL+listViewItem.getProfileaddress()).into(img);

            title.setText(listViewItem.getUser_name());
            Context.setText(listViewItem.getMessage());
        }


        return convertView;
    }

    public void addVO(FriendListVO f_List){

        listVO.add(f_List);
    }
}