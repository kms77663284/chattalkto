package com.example.talktoclient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.IBinder;
import android.os.StrictMode;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.data.ChatVO;
import com.data.DataBases;
import com.data.SharedPreferenceUtil;
import com.data.TalkToDBOpenHelper;
import com.data.UserVO;
import com.data.DataPacket;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatActivity extends AppCompatActivity {
    private ListView chat = null;
    private Button btn = null;
    private ChatAdapter adapter = null;
    private EditText content = null;
    ClientService cs;
    boolean isService = false;
    private UserVO me;
    private SharedPreferenceUtil session;
    private int ROOMID;
    //private ChatDBHelper cdb;
    private TalkToDBOpenHelper tdb;
    private SQLiteDatabase chatdb;

    private ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            ClientService.ClientBinder cb = (ClientService.ClientBinder)service;
            cs = cb.getService();
            isService = true;
        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            isService = false;
        }
    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            DataPacket data = (DataPacket)intent.getSerializableExtra("chat");
            adapter.addVO(data.getCdata());
            adapter.notifyDataSetChanged();
            chat.setSelection(adapter.getCount()-1);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        tdb = new TalkToDBOpenHelper(getApplicationContext(), null, 1);
        chatdb = tdb.getWritableDatabase();

        session = new SharedPreferenceUtil(getApplicationContext());
        me = new UserVO();
        me.setID(session.getSharedID());
        me.setName(session.getSharedName());

        Intent receiveIntent = getIntent();
        ROOMID = receiveIntent.getIntExtra("roomid", -1);

        chat = (ListView)findViewById(R.id.chatview);
        adapter = new ChatAdapter();
        chat.setAdapter(adapter);
        content = (EditText)findViewById(R.id.content);

        btn = (Button)findViewById(R.id.send);
        btn.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (!content.getText().toString().equals("")) {
                    ChatVO chat = new ChatVO();
                    chat.setTime(new SimpleDateFormat("yyyy/MM/dd HH:mm").format(new Date(System.currentTimeMillis())));
                    chat.setContent(content.getText().toString());
                    chat.setUser(me);
                    cs.sendMSG(chat, ROOMID);
                    content.setText("");
                }
            }
        });

        Intent serviceIntent = new Intent(ChatActivity.this, ClientService.class);
        bindService(serviceIntent, conn, Context.BIND_AUTO_CREATE);

        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, new IntentFilter("receivechat"));
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
        unbindService(conn);
    }

    @Override
    protected void onPause() {
        super.onPause();
        adapter.clean();
        adapter.notifyDataSetChanged();
    }


    @Override
    protected void onResume() {
        SyncChat();
        super.onResume();
    }

    public void SyncChat(){
        String sql = "select * from " + DataBases.ChatDB.TABLE + " where " + DataBases.ChatDB.RN + "=" + ROOMID + " order by " + DataBases.ChatDB.TIME;
        Cursor result =  chatdb.rawQuery(sql, null);
        while(result.moveToNext()) {
            ChatVO data = new ChatVO();
            UserVO user = new UserVO();
            user.setID(result.getString(result.getColumnIndex(DataBases.ChatDB.UID)));
            user.setName(result.getString(result.getColumnIndex(DataBases.ChatDB.NAME)));
            data.setUser(user);
            data.setContent(result.getString(result.getColumnIndex(DataBases.ChatDB.CONTENT)));
            data.setTime(result.getString(result.getColumnIndex(DataBases.ChatDB.TIME)));
            data.setMe(user.getID().equals(session.getSharedID()));
            adapter.addVO(data);
        }
        result.close();
        adapter.notifyDataSetChanged();
        chat.setSelection(adapter.getCount()-1);
    }
}
