package com.example.talktoclient;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.server.ServerData;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class sign_in extends AppCompatActivity {
    private static final String  URL = "http://"+ ServerData.IP+":"+ServerData.JSPPORT+"/TalkTo/signin.jsp";
    private EditText userid;
    private EditText username;
    private EditText password;
    private EditText phoneNumber;
    private Button btn1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        userid = (EditText)findViewById(R.id.userid);
        username = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        phoneNumber = (EditText)findViewById(R.id.phoneNumber);
        btn1 = (Button)findViewById(R.id.btn1);

        Intent intent = getIntent();

        btn1.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("btn1","클릭");
                String id = userid.getText().toString();
                String pwd = password.getText().toString();
                String user_name = username.getText().toString();
                String phoneNum = phoneNumber.getText().toString();
                if(id.equals("")||pwd.equals("")||user_name.equals("")||phoneNum.equals("")){
                    Toast.makeText(v.getContext(),"입력을 확인해주세요.",Toast.LENGTH_SHORT);
                    return;
                }
                ContentValues values= new ContentValues();
                values.put("msg","signin");
                values.put("id",id);
                values.put("pwd",pwd);
                values.put("user_name",user_name);
                values.put("phoneNum",phoneNum);

                NetworkTask networkTask = new NetworkTask(sign_in.this,URL, values);
                networkTask.execute();
            }
        });
    }


}
