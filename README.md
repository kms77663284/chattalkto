# Talk To

카카오톡을 보고 저만의 채팅 어플 개발기간 : 2019.11.25 ~ 12. 8(13일)

<img src="https://gitlab.com/kms77663284/chattalkto/raw/master/doc/talktogif.gif">


## 개발환경
- Android (Java)
- Apache Tomcat8.5
- JSP
- JAVA SOCKET
- Maria DB

## 구조

- 회원가입  -   로그인   -    친구 목록       -    채팅 목록   -    채팅 창   
-  친구추가 , 대화상대추가, #버스,#지하철,#날씨 기능(채팅시)
-(web server)  JSON 데이터를 통신
-(java server) 동기화 다중쓰레드 채팅방

## 어플 기능
* 회원가입 로그인 프로필수정 - JSON 으로 HTTP 통신으로 웹서버로 통신
* 채팅 - 데이터를 SOCKET 통신으로 JAVA 서버로 통신( 실시간, 동기화)

## 담당 역활
1. 어플 기획
2. 회원가입 로그인 친구목록 웹(HTTP connection)통신으로 구현
3. 안드로이드 java server 연결 구현
4. java server 채팅방 구현
5. 데이터베이스 간단한 설계
 





