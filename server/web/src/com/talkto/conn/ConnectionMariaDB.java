package com.talkto.conn;

import java.sql.Connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ConnectionMariaDB {
	private static DataSource dataSource;
	private static Connection conn;
	
	private ConnectionMariaDB(){}
	
	public static Connection getConnection() {
		if (conn == null) {
			try {
				Context context = new InitialContext();
				dataSource = (DataSource) context.lookup("java:comp/env/jdbc_mariadb");
				conn = dataSource.getConnection();		
				System.out.println("DB연결 성공");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}
		return conn;
	}
}
