package com.server;

public class MessageProtocol{

	// 프토로콜들의 정의
	public static final int CHAT_CREATE = 1000; // 1대1 채팅방 생성 (클라이언트가 서버에게)
	public static final int SOCKET_CONNECT= 1001; //소켓 처음연결시 정보 전달 프로토콜 (클라이언트가 서버에게)
	public static final int SOCKET_CONNECT_SUCCESS = 1002; // 소켓 연결 성공 통보
	public static final int SC_ROOMDATA_SEND = 2000; //서버에서 클라이언트로 룸정보 보내기
	public static final int CS_SEND_MSG=2011; //클라이언트가 서버에게 메세지를 보냄
	public static final int SC_SEND_MSG=2012; //서버가 클라이언트에게 메세지를 보낸다
	public static final int CHAT_OUT = 1003;
	public static final int SYNC_CHATROOM = 3000;
	public static final int SYNC_CHAT = 3001;
	public static final int SYNC_START = 3003;
	public static final int SYNC_DONE = 3002;
	public static final int CHAT_IN=1005;
}