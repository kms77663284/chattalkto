package com.data;

import java.io.Serializable;
import java.util.ArrayList;

public class DataPacket implements Serializable  {
	private int protocol; // 통신 프로토콜
	private UserVO udata; // 유저 데이터
	private ChatVO cdata; // 채팅 데이터
	private ArrayList<UserVO> Userlist; // 채팅방 유저 목록
	private int roomId; //채팅방 고유번호
	public int getProtocol() {
		return protocol;
	}
	public void setProtocol(int protocol) {
		this.protocol = protocol;
	}
	public UserVO getUdata() {
		return udata;
	}
	public void setUdata(UserVO udata) {
		this.udata = udata;
	}
	public ChatVO getCdata() {
		return cdata;
	}
	public void setCdata(ChatVO cdata) {
		this.cdata = cdata;
	}
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public ArrayList<UserVO> getUserlist() {
		return Userlist;
	}
	public void setUserlist(ArrayList<UserVO> userlist) {
		Userlist = userlist;
	}
	
	
}
