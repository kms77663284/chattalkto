package com.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.logic.ChatRoomManager;
import com.logic.UserManager;
import com.server.Server;


public class DAO {
	private static final String driverClassName="org.mariadb.jdbc.Driver";
	private static final String url="jdbc:mariadb://localhost:3306/talkto";
	private static final String username="root";
	private static final String password="1234";
	private Connection conn = null;
	private PreparedStatement pstmt = null;
	private ResultSet rs = null;
	
	private static DAO instance;
	
	private DAO(){
		try {
			Class.forName(driverClassName);
			conn = DriverManager.getConnection(url, username, password);
		}catch(ClassNotFoundException e) {
			System.out.println("socket server db load fail");
			e.printStackTrace();
		}catch(SQLException e) {
			System.out.println("socket server db access fail");
			e.printStackTrace();
		}
	}
	
	public static DAO getInstance() {
		if (instance == null) instance = new DAO();
		return instance;
	}
	
	public void initServer() {
		try {
			String sql = "select * from users";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			while (rs.next()) {
				UserVO tmp = new UserVO();
				tmp.setID(rs.getString("id"));
				tmp.setName(rs.getString("user_name"));
				tmp.setPhone(rs.getString("phoneNum"));
				UserManager.addUser(tmp, null, null);
			}
			
			sql = "select * from chatlist";
			pstmt = conn.prepareStatement(sql);
			rs = pstmt.executeQuery();
			
			int maxRoomnum = -1;
			
			while (rs.next()) {
				ChatRoom tmp = new ChatRoom();
				tmp.setId(rs.getInt("rnum"));
				String users = rs.getString("ulist");
				String[] list = users.split(",");
				ArrayList<User> ulist = new ArrayList<User>();
				for (String key : list) {
					ulist.add(Server.users.get(key));
				}
				tmp.setUserList(ulist);
				Server.roomList.put(tmp.getId(), tmp);
				if (maxRoomnum < tmp.getId()) maxRoomnum = tmp.getId();
			}
			
			ChatRoomManager.InitRoomnum(maxRoomnum+1);
			
		}catch(SQLException e) {
			System.out.println("Server init fail");
			e.printStackTrace();
		}
	}
	
	public void addChatRoom(ChatRoom data) {
		int roomid = data.getId();
		StringBuilder bd = new StringBuilder();
		for (User tmp : data.getUserList()) {
			if (bd.length() == 0) {
				bd.append(tmp.getUdata().getID());
			}else {
				bd.append(","+tmp.getUdata().getID());
			}
		}
		try {
			String sql = "insert into chatlist(rnum, ulist) values(?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, roomid);
			pstmt.setString(2, bd.toString());
			pstmt.executeUpdate();
		}catch(SQLException e) {
			System.out.println("chatroom add fail");
			e.printStackTrace();
		}
	}
	
	public int checkChatRoom(UserVO call, ArrayList<UserVO> list) {
		ArrayList<UserVO> test = new ArrayList<UserVO>(list);
		test.add(call);
		StringBuffer buf = new StringBuffer();
		for (UserVO tmp : test) {
			if (buf.length() == 0) buf.append(tmp.getID());
			else buf.append(","+tmp.getID());
		}
		try {
			String sql = "select rnum from chatlist where ulist=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, buf.toString());
			rs = pstmt.executeQuery();
			if (rs.next()) {
				return rs.getInt("rnum");
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	public int addChat(int RID, ChatVO data) {
		int CID = -1;
		try {
			String sql = "insert into chat(rnum, userid, content, ctime) values(?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, RID);
			pstmt.setString(2, data.getUser().getID());
			pstmt.setString(3, data.getContent());
			pstmt.setString(4, data.getTime());
			pstmt.executeUpdate();
			
			sql = "select cid from chat where rnum=? and userid=? and content=? and ctime=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, RID);
			pstmt.setString(2, data.getUser().getID());
			pstmt.setString(3, data.getContent());
			pstmt.setString(4, data.getTime());
			rs = pstmt.executeQuery();
			if (rs.next())
				CID = rs.getInt("cid");
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return CID;
	}
	
	public ArrayList<ChatVO> loadChat(int id){
		ArrayList<ChatVO> list = new ArrayList<ChatVO>();
		try {
			String sql = "select * from chat where rnum = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, id);
			rs = pstmt.executeQuery();
			while(rs.next()) {
				ChatVO tmp = new ChatVO();
				tmp.setTime(rs.getString("ctime"));
				tmp.setContent(rs.getString("content"));
				tmp.setUser(UserManager.getUser(rs.getString("userid")).getUdata());
				tmp.setCID(rs.getInt("cid"));
				list.add(tmp);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public ArrayList<ChatRoom> loadChatRoom(String id) {
		ArrayList<ChatRoom> list = new ArrayList<ChatRoom>();
		try {
			String sql = "select * from chatlist where ulist like ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, "%"+id+"%");
			rs = pstmt.executeQuery();
			while (rs.next()) {
				ChatRoom tmp = new ChatRoom();
				tmp.setId(rs.getInt("rnum"));
				String ulist = rs.getString("ulist");
				String[] users = ulist.split(",");
				tmp.setUserList(new ArrayList<User>());
				for (String key : users) {
					tmp.getUserList().add(UserManager.getUser(key));
				}
				list.add(tmp);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public void removeChatRoom(int id) {
		try {
			String sql = "delete from chatroom where roomid=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(0, id);
			pstmt.executeUpdate();
		}catch(SQLException e) {
			System.out.println("chatroom remove fail");
			e.printStackTrace();
		}
	}
}
