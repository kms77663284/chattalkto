package com.data;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.ArrayList;

import com.logic.ChatRoomManager;
import com.server.MessageProtocol;
import com.server.Server;


public class ChatRoom {
	private int id; // 룸 ID
    private ArrayList<User> userList;
    private DAO db;
    
    public ChatRoom() {
    	db = DAO.getInstance();
    }
    
    public void broadcast(DataPacket data) {
    	int CID = db.addChat(id, data.getCdata());
    	data.setProtocol(MessageProtocol.SC_SEND_MSG);
    	data.getCdata().setCID(CID);
    	for (User user: userList) {
    		try {
				ObjectOutputStream oos = user.getOos();
				if (oos != null) oos.writeObject(data);
			} catch (IOException e) {
				System.out.println("메세지 전송 에러");
				e.printStackTrace();
			}
    	}
    }
    
    public void chatout(DataPacket data){
    	int find = 0;
    	for (int i=0; i < userList.size(); i++) {
    		User user = userList.get(i);
    		if (user.getUdata().getID().equals(data.getUdata().getID())) find = i;
    		try {
				ObjectOutputStream oos = user.getOos();
				if (oos != null) oos.writeObject(data);
			} catch (IOException e) {
				System.out.println("채팅방 나감 전달 에러");
				e.printStackTrace();
			}
    	}
    	userList.remove(find);
    	if (userList.size() == 0) {
    		ChatRoomManager.deleteRoom(id);
    	}
    }
    
	public int getId() {
		return id;
	}
	
	public ArrayList<User> getUserList() {
		return userList;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}
	
	public void Notify() {
		DataPacket data = new DataPacket();
		ArrayList<UserVO> list = new ArrayList<UserVO>();
		for (User tmp : userList) {
			list.add(tmp.getUdata());
		}
		data.setUserlist(list);
		data.setRoomId(id);
		data.setProtocol(MessageProtocol.CHAT_CREATE);
		for (User tmp : userList) {
			try {
				ObjectOutputStream oos = tmp.getOos();
				if (oos != null) oos.writeObject(data);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public ArrayList<UserVO> getJustUsers() {
		ArrayList<UserVO> list = new ArrayList<UserVO>();
		for (User tmp : userList) {
			list.add(tmp.getUdata());
		}
		return list;
	}
}
