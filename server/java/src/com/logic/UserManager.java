package com.logic;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.data.User;
import com.data.UserVO;
import com.server.Server;

public class UserManager {
	public static void addUser(UserVO udata, ObjectInputStream ois, ObjectOutputStream oos) {
		synchronized (Server.users) {
			if (getUser(udata.getID()) == null) {
				User newUser = new User(udata, oos, ois);
				Server.users.put(udata.getID(), newUser);
			}
			else {
				getUser(udata.getID()).setOos(oos);
				getUser(udata.getID()).setOis(ois);
			}
		}
	}
	public static User getUser(String ID) {
		return Server.users.get(ID);
	}
}
